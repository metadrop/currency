<?php

/**
 * @file
 * Contains class \BartFeenstra\Currency\Exception\AmountNotNumericException.
 */

namespace BartFeenstra\Currency\Exception;

/**
 * Describes an amount string that is not numeric.
 */
class AmountNotNumericException extends \Exception {}
