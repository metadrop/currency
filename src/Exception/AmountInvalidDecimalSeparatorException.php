<?php

/**
 * @file
 * Contains class \BartFeenstra\Currency\Exception\AmountInvalidDecimalSeparatorException.
 */

namespace BartFeenstra\Currency\Exception;

/**
 * Describes an amount string that has invalid decimal separators.
 */
class AmountInvalidDecimalSeparatorException extends \Exception {}
